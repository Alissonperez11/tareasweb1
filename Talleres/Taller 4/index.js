function mostrarHora() {
  let hora = new Date().getHours();
  let minuto = new Date().getMinutes();
  let segundo = new Date().getSeconds();
  let documento = document.getElementById("punto1");
  let horaNueva = hora * 3600 + minuto * 60 + segundo;
  documento.innerHTML = `La hora actual es, ${hora}:${minuto}:${segundo}.......${horaNueva} segundos.`;
}

function calcularArea() {
  const base = document.getElementById("base").value;
  const altura = document.getElementById("altura").value;
  const r = (base * altura) / 2;
  const resultado = document.getElementById("resultado");
  resultado.innerHTML = `El area de triangulo es, ${r}`;
}

function calcularRaiz() {
  const base = document.getElementById("numero").value;
  const resultado = document.getElementById("resultadoRaiz");
  if (base % 2 == 0) {
    resultado.innerHTML = `Ingrese un numero IMPAR por favor.`;
  } else {
    const r = Math.sqrt(base);
    resultado.innerHTML = `La raiz cuadrada del numero ${base} es, ${r.toFixed(
      3
    )}.`;
  }
}

function calcularPalabra() {
  const base = document.getElementById("palabra").value;
  const resultado = document.getElementById("resultadoPalabra");
  resultado.innerHTML = `La palabra "${base}" tiene ${base.length} letras.`;
}
function mostrarNavegador() {
  let getBrowserInfo = function () {
    var ua = navigator.userAgent,
      tem,
      M =
        ua.match(
          /(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i
        ) || [];
    if (/trident/i.test(M[1])) {
      tem = /\brv[ :]+(\d+)/g.exec(ua) || [];
      return "IE " + (tem[1] || "");
    }
    if (M[1] === "Chrome") {
      tem = ua.match(/\b(OPR|Edge)\/(\d+)/);
      if (tem != null) return tem.slice(1).join(" ").replace("OPR", "Opera");
    }
    M = M[2] ? [M[1], M[2]] : [navigator.appName, navigator.appVersion, "-?"];
    if ((tem = ua.match(/version\/(\d+)/i)) != null) M.splice(1, 1, tem[1]);
    return M.join(" ");
  };
  const resultado = document.getElementById("resultadoNav");
  resultado.innerHTML = `${getBrowserInfo()}`;
}

function mostrarPantalla() {
  const resultado = document.getElementById("resultadoPa");
  const alto =document.body.offsetHeight;
  const ancho =document.body.offsetWidth;
  resultado.innerHTML = `el alto de la pantalla es, "${alto}" y el ancho es, "${ancho}".`;
}
