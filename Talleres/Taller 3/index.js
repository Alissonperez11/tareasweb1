function mostrar() {
  var numero1 = document.getElementById("n1").value;
  var numero2 = document.getElementById("n2").value;
  var res = document.getElementById("res");
  if (numero1 == "" || numero2 == "") {
    res.innerHTML = "Que los datos no esten vacios";
  } else {
    res.innerHTML="";
    let resultado = 0;
    let operacion = "";
    for (let i = 1; i < 6; i++) {
      switch (i) {
        case 1:
          resultado = parseInt(numero1) + parseInt(numero2);
          operacion = "+";
          break;
        case 2:
          resultado = numero1 - numero2;
          operacion = "-";
          break;
        case 3:
          resultado = numero1 * numero2;
          operacion = "*";
          break;
        case 4:
          resultado = numero1 / numero2;
          operacion = "/";
          break;
        case 5:
          resultado = numero1 % numero2;
          operacion = "%";
          break;
      }
      alert(`${numero1} ${operacion} ${numero2} = ${resultado}.`);
      
    }
  }
}
